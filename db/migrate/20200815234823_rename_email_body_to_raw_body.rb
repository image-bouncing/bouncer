class RenameEmailBodyToRawBody < ActiveRecord::Migration[6.0]
  def change
    rename_column :emails, :body, :raw_body
  end
end
