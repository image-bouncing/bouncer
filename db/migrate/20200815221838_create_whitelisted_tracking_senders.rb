class CreateWhitelistedTrackingSenders < ActiveRecord::Migration[6.0]
  def change
    create_table :whitelisted_tracking_senders do |t|
      t.string :email
      t.timestamps
    end
  end
end
