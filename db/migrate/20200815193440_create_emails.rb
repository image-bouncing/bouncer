class CreateEmails < ActiveRecord::Migration[6.0]
  def change
    create_table :emails do |t|
      t.string :to
      t.string :from
      t.datetime :received_at
      t.string :body
      t.string :subject

      t.timestamps
    end
  end
end
