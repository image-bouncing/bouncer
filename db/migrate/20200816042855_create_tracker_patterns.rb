class CreateTrackerPatterns < ActiveRecord::Migration[6.0]
  def change
    create_table :tracker_patterns do |t|
      t.string :pattern
      t.string :name

      t.timestamps
    end
  end
end
