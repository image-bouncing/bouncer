# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

TrackerPattern.create(name: 'Sneaky Pixel - CD', pattern: 'sneaky-pixel.chrisdurheim.com')
TrackerPattern.create(name: 'Mailerlite', pattern: 'click.mlsend.com/link')
