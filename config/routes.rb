Rails.application.routes.draw do
  root 'emails#index'
  resources :emails, only: [:index, :show]
  resources :tracker_patterns, only: [:index, :new, :create, :edit, :update, :destroy]
  resources :whitelisted_tracking_senders,
            only: [:create, :destroy],
            param: :email,
            constraints: { email: /.+(@|(%40)).+\..*/ }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
