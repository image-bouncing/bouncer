class ApplicationMailbox < ActionMailbox::Base
  routing 'out@bouncer.chrisdurheim.com' => :tracker_bounced_emails
end
