class TrackerBouncedEmailsMailbox < ApplicationMailbox
  def process
    Email.create(
      subject: mail.subject,
      to: mail.to.join(', '),
      from: mail.from.join(', '),
      received_at: mail.date,
      body: body
    )
  end

  def body
    if mail.multipart? && mail.html_part
      document = Nokogiri::HTML(mail.html_part.body.decoded)
      document.at_css("body").inner_html.encode('utf-8')
    elsif mail.multipart? && mail.text_part
      mail.text_part.body.decoded
    else
      mail.decoded
    end
  end
end
