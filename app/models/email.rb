class Email < ApplicationRecord
  def from_whitelisted_tracking_sender?
    @from_whitelisted_tracking_sender ||=
      WhitelistedTrackingSender.all.pluck(:email).include?(from)
  end

  def tracker_count
    @tracker_count ||= begin
      body
      @tracker_count
    end
  end

  def body
    @body ||= begin
      @tracker_count = 0
      TrackerPattern.pluck(:pattern).each do |known_tracker|
        tracker_pattern = "//img[contains(@src, \"#{known_tracker}\")]"
        body_as_html.xpath(tracker_pattern).each do |tracker_tag|
          bounce_tracker(tracker_tag) unless from_whitelisted_tracking_sender?
          @tracker_count += 1
        end
      end
      body_as_html.serialize(save_with: 0)
    end
  end

  def body_as_html
    @body_as_html ||= Nokogiri::HTML(raw_body) do |config|
      config.options = Nokogiri::XML::ParseOptions::NOBLANKS
    end
  end

  def bounce_tracker(tracker_tag)
    tracker_tag['data-src'] = tracker_tag['src']
    tracker_tag['data-target'] = 'bouncer.tracker'
    tracker_tag.remove_attribute('src')
    tracker_tag.add_class('blocked-tracker')
  end
end
