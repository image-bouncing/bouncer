class WhitelistedTrackingSendersController < ApplicationController
  def create
    WhitelistedTrackingSender.create(create_params)
  end

  def destroy
    WhitelistedTrackingSender.find_by(email: params[:email]).destroy
  end

  private

  def create_params
    params.permit(:email)
  end
end
