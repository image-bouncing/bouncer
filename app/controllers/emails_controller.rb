class EmailsController < ApplicationController
  def index
    @emails = Email.all.order(received_at: :desc)
  end

  def show
    @email = Email.find(params[:id])
  end
end
