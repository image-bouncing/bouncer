class TrackerPatternsController < ApplicationController
  def index
    @tracker_patterns = TrackerPattern.all
  end

  def show
    @tracker_pattern = TrackerPattern.find(params[:id])
  end

  def new
    @tracker_pattern = TrackerPattern.new
  end

  def create
    @tracker_pattern = TrackerPattern.new(create_params)

    if @tracker_pattern.save
      redirect_to tracker_patterns_path
    else
      flash.now[:error] = 'Could not create tracker pattern'
      render action: 'new'
    end
  end

  def edit
    @tracker_pattern = TrackerPattern.find(params[:id])
  end

  def update
    @tracker_pattern = TrackerPattern.find(params[:id])
    if @tracker_pattern.update(update_params)
      redirect_to tracker_patterns_path
    else
      flash.now[:error] = 'Could not update tracker pattern'
      render action: 'edit'
    end
  end

  def destroy
    @tracker_pattern = TrackerPattern.find(params[:id])
    @tracker_pattern.destroy
    redirect_to tracker_patterns_path
  end

  private

  def create_params
    params.require(:tracker_pattern).permit(:name, :pattern)
  end

  def update_params
    create_params
  end
end
