import { Controller } from "stimulus"

import Rails from "@rails/ujs";

export default class extends Controller {
  static targets = [ "tracker", "fromAddress", "trackerNotification" ]

  override() {
    this.loadTrackers()

    // store whitelist entry for tracker
    const formData = new FormData()
    formData.append('email', this.fromAddressTarget.innerHTML)

    Rails.ajax({
      type: "post",
      url: "/whitelisted_tracking_senders/",
      data: formData
    })

    // Update UI
    const notificationPanel = this.trackerNotificationTarget
    notificationPanel.innerHTML = "Got it! We'll allow trackers from this sender going forward"
    notificationPanel.classList.add('tracker-notification--whitelisted')
  }

  loadOnce() {
    this.loadTrackers()

    // Update UI
    const notificationPanel = this.trackerNotificationTarget
    notificationPanel.innerHTML = "Got it! We've loaded trackers this one time. We'll still block trackers from this sender in the future"
    notificationPanel.classList.add('tracker-notification--whitelisted')
  }

  loadTrackers() {
    const trackers = this.trackerTargets
    trackers.forEach((tracker) => {
      tracker.src = tracker.dataset.src
    })
  }

  rebounce() {
    // bounce tracker images
    const trackers = this.trackerTargets
    trackers.forEach((tracker) => {
      tracker.dataset.src = tracker.src
      delete tracker.src
    })

    // delete whitelist entry for tracker
    const email = this.fromAddressTarget.innerHTML

    Rails.ajax({
      type: "delete",
      url: `/whitelisted_tracking_senders/${email}`,
    })

    // Update UI
    const notificationPanel = this.trackerNotificationTarget
    notificationPanel.innerHTML = "Got it! We'll block trackers from this sender going forward"
    notificationPanel.classList.remove('tracker-notification--whitelisted')
  }
}
