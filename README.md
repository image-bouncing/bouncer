# Welcome to Bouncer

## What's Bouncer?

Bouncer is an example implementation of the Asset Bouncer privacy approach to tracking pixels/assets.

> Make sure to check out the detailed description/proposal for [Asset Bouncing](./ASSET-BOUNCING.md)

Bouncer acts as the tough guy at the door, keeping tracking pixels out of your emails unless, of course, they're "on the list".

## Usage

This application is intended to demonstrate the concept of Asset Bouncing - in particular with image tracking pixels.

Note: any email content emailed to an `*@bouncer.chrisdurheim.com` email address may be parsed and made publicly available here. Don't send anything you don't want the world to be aware of!

# Using Sneaky Pixel

## Set Up

```
# Get the repo

$ git clone git@gitlab.com:image-bouncing/bouncer.git

# change directory and install the gems for the app

$ cd bouncer
$ bundle install

# set up the database for the app

$ bundle exec rails db:create
$ bundle exec rails db:migrate

# start the app server

$ bundle exec rails s
```

Navigate to [localhost:3000](http://localhost:3000/) to see an email inbox.

# Ingesting a Tracker-laden Email

For actual email ingest and bouncing, check out the demo version of this app at https://bouncer.chrisdurheim.com

1. Send an email to out@bouncer.chrisdurheim.com; include one or more asset tracking tags (try out https://www.gitlab.com/image-bouncing/sneaky-pixel)
2. Head to https://bouncer.chrisdurheim.com to see the inbox of emails that have been ingested; find the email you had sent and open it

Note that the image tracking pixel is not loaded (check Sneaky Pixel to make sure).

Note that the email render indicates that the tracking pixel has been identified and gives options on how to process

## Allowing trackers

Click 'Allow Trackers for <this sender>' and note that the tracker has now loaded (see Sneaky Pixel stats)

## Blocking trackers

Once trackers have been allowed for an individual sender, (re-)open an email from that sender and you'll be presented with an option to block the sender again.

# How Bouncer Works

Bouncer processes email at the time of rendering. For emails that are from a non-whitelisted sender, the bouncing process checks the email content for any image tags matching the patterns from `TrackingPatterns`.

For any matches that are found, the `img` tag is left in the body, but the `src` attribute is removed and the contents moved to a `data-src` attribute.

This prevents actual requesting of the tracking pixel, saving the user from contributing to the pixel tracking counts.

If a user opts to "allow" trackers, the `src` attribute is repopulated for each image tag and the sender's email address is added to the whitelist.

## Known Tracker Patterns

To administer known tracker patterns, simply visit the `Known Tracker Patterns` page (available via link from the root path).

Each mailing list management provider has their own mechanism to create tracking pixels, so an entry can be created here for any individual tracker to be added to the filter list.

Running `rails db:seed` will populate the Tracker Pattern list with two starting options:
- `click.mlsend.com/link` (for Mailerlite)
- `sneaky-pixel.chrisdurheim.com` (for Sneaky Pixel)

Additional tags may be added for other providers, such as Mailchimp and ConvertKit.
