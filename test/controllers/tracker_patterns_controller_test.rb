require 'test_helper'

class TrackerPatternsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get tracker_patterns_index_url
    assert_response :success
  end

  test "should get show" do
    get tracker_patterns_show_url
    assert_response :success
  end

  test "should get new" do
    get tracker_patterns_new_url
    assert_response :success
  end

  test "should get create" do
    get tracker_patterns_create_url
    assert_response :success
  end

  test "should get edit" do
    get tracker_patterns_edit_url
    assert_response :success
  end

  test "should get update" do
    get tracker_patterns_update_url
    assert_response :success
  end

  test "should get destroy" do
    get tracker_patterns_destroy_url
    assert_response :success
  end

end
