require 'test_helper'

class WhitelistedTrackingSendersControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get whitelisted_tracking_senders_create_url
    assert_response :success
  end

  test "should get delete" do
    get whitelisted_tracking_senders_delete_url
    assert_response :success
  end

end
