## How Does Asset Bouncing Work?

Here's how Bouncer works:
1. When an email is received it is scanned for asset tags matching known asset tracking naming patterns.
2. The email content is adjusted with placeholder tags for any "suspicious" assets that may be trackers. Non-suspicious asset tags are left alone.
3. When the recipient views the email, they are given an indicator that a tracking pixel may be present and given a choice:
  - Do nothing (keep the pixel blocked but don't set any future rules)
  - Show asset for this email (in case the asset was falsely identified as a tracker, this would fetch the asset and render it)
  - Always show for this sender (in the case that the sender is one that the recipient is ok with tracking being enabled for)

### When Would I Want To Allow Tracking Pixels?

Privacy on the web is an important consideration for your customers. But so is _control_. You may not want Walmart knowing you opened up their email ad for romance novels, but allowing your favorite blogger to know that you're staying engaged might be ok in your book.

### Comparison with Asset Proxying

Asset proxying indicates 'open' for every asset tracker immediately on receipt by the server (by accessing each asset on email receipt). This gives the sender an inflated open rate.

Unfortunately, this can be a _reward_ for some mailing list managers as open rate is often used to value an email list for placement of sponsored content.

Asset Bouncing does just the opposite - it indicates 'unopened' for every asset tracker by refusing to download the asset unless permission is explicitly granted by the user.

This can help give customers control over how they reward the mailing list owners they interface with.

### Cons to Asset Bouncing

Asset Bouncing has benefits in terms of user control but should be measured against Asset Proxying depending on the needs of your user base.

Asset Bouncing requires maintenance of a list of suspicious patterns to flag for asset tags - otherwise it may be susceptible to a high number of false positives. As the list of open tracking methods increases, Asset Bouncing requires manual work to keep up-to-date, which may not be cost effective.

### Hybrid Options

It's certainly possible to combine Asset Bouncing with Asset Proxying to attempt to get the "best of both worlds". Here are some options:

1. Asset Bounce for the first email from a particular sender and allow the recipient to set a rule for how to handle potential tracking assets going forward for that sender (proxy, accept always, bounce always)
2. Asset Proxy with bounce-based filter. In this scenario, all assets would be run through the filter for any "likely trackers". Assets that are not suspected trackers would be proxied immediately. Assets that are suspected trackers would be run through the bouncer - with user control for single approval, ongoing approval, or ongoing rejection of the trackers.

### Recommendation

Option 2 of the Hybrid Options seems to be the most robust solution and worth the tradeoff of implementation cost for giving users more control.

By using Asset Proxy for all non-suspicious assets, users gain IP anonymity from list owner tracking.

By sniffing out suspected trackers, users gain the ability to control their desired interaction with the list owner - either allowing them to collect metrics via trackers or not.

### Conclusion

Asset Bouncing can be used effectively as an individual privacy strategy or as a complement to asset proxying.

From an implementation perspective, there would be economies of scale to open-sourcing this approach - as it would spread the maintenance cost of identifying tracking asset patterns across a much broader set of contributors.
